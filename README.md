# ~/.dotfiles

This repo contains my personal dotfiles. I copy stuffs from several people and I personalize those to go better with my workflow. I remain open to any improvement ! And you are free to clone and to adapte to your sauce.

## My setup

### OS:
* MacOS for most of things
* Ubuntu and ArchLinux for servers/tinkering

### Editors:
* vscode
* neovim

### Shell:
* ZSH

Link to [my /uses page](https://igorvisi.com/uses)


## Installation
Good to know beforehand, I use :
* [dotbot](github.com/anishathalye/dotbot) to manage my dotfiles.
* [exa](https://github.com/ogham/exa) as a ls remplacement.
* [bat](https://github.com/sharkdp/bat) as a cat remplacement.

As well as many other tools...

### Clone and configure env variables
```bash
git clone https://github.com/igorvisi/dotfiles ~/.dotfiles
```
Configure according to you
~/.dotfiles/shell/global
~/.gitconfig.local

### Install conf.
```bash
cd ~/.dotfiles
chmod +x install
./install
```

## Screenshots

<p align="center"><a name="top" href="https://github.com/igorvisi/dotfiles"><img src="https://github.com/igorvisi/dotfiles/raw/master/Screenshot2.png" alt="Preview" width="50%"></a><a name="top" href="https://github.com/igorvisi/dotfiles"><img src="https://github.com/igorvisi/dotfiles/raw/master/Screenshot1.png" alt="Preview" width="50%"></a></p>